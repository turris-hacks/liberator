# Premise

I used logs from previous __Turris__ speed test tool to report ISP connection problems.
Now with librespeed, it is little complicated.
For those who like to complain (like me) to ISP that provided (upload/download) speed is not as advertised, first step is to collect some data.

!!!
    Not definitive solution, just fast hack for my personal use, until there's more time
    to implement in __foris-controler/reForis__ module.


## Collection of data

Initial speed tool had the ability to collect data online. That is not what you want.
This tools collect data using storage mounted to your router. It is your data now.

## Data processing

This is open for now. Basic idea is to migrate the json to sqlite database later. I may do
some fast hack and I'm not sure i will share that. Hopefully we can make it as part of
__foris-controller/reForis__.

Open to ideas.

## roadplan

- [x] collect data
- [ ] storage (sqlite?)
- [ ] generate report for ISP

## Manual

### defaults

- target ``/srv/liberator``
- run test every 20 minutes

### installation

Copy scripts to according paths on your router.

```
/
├── etc
│   └── cron.d
│       └── liberator
└── usr
    └── libexec
        └── liberator.sh
```

!!! tip
    do not copy README.md and LICENSE.md

Please do report any ideas/bugs/errors either on [turris forum](https://forum.turris.cz/t/librespeed-results-preservation/19401) or make ticket here.


!!! warning
    Any errors you have are on your own responsibility, this utility is not supported by __Turris Tech__ support.
