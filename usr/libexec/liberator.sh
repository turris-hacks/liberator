# GPLv3 2023

# target folder, optional is to have usb stick
TARGET=$1

# may change to suit your need
FOLDER="$1/liberator"

# target is required
if [[ ! -d "$FOLDER" ]]; then
    mkdir $FOLDER
elif [ $# -eq 0 ]; then
    echo 'Provide an argument!'
    exit 1
fi

# you may change default result prefix
if [[ "$2" ]]; then
    PREFIX="$2"
else
    PREFIX="speedtest"
fi

# you may use local, i use UTC
# specify to ISP
TIME=$(date -u "+%Y-%m-%d_%H_%M")
FILENAME="${PREFIX}_${TIME}.json"

EXECUTABLE=$(which librespeed-cli)

$EXECUTABLE --json | python -m json.tool > "$FOLDER/$FILENAME"

exit 0
